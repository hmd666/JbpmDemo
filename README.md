**JBPM**
========
#### 介绍
JBPM是JBOSS下的一个开源java工作流项目，该项目提供eclipse插件，基于Hibernate实现数据持久化存储。
#### JBPM开发环境搭建
一、软件下载
1. eclipse  <br>
下载地址：http://www.eclipse.org/downloads/ <br>
2. jbpm6.5 Installer  <br>
下载地址：http://www.jbpm.org/download/download.html <br>

二、插件工具安装 
1. eclipse安装  <br>
eclipse直接解压即可 <br>
2. 安装jBPM Eclipse plugin <br>
打开eclipse，Help -> Install New Software -> Add -> Archive，选择jbpm-installer文件夹 -> lib -> org.drools.updatesite-6.5.0.Final.zip <br>
点击Select All，一路点击next完成安装即可，最后重启eclipse。<br>
3. 安装 Eclipse BPMN 2.0 Modeler <br>
Eclipse BPMN2.0 Modeler是eclipse流程设计插件，通过此插件我们可以可视化设工作流程。启动eclipse 点击Help—Install NewSoftware 在work with中输入插件安装地址。<br>
全选，一路点击next即可完成安装。 <br>
注：不同eclipse版本插件安装地址: <br>
    Eclipse 3.6 (Helios): http://download.eclipse.org/bpmn2-modeler/updates/helios <br>
    Eclipse 3.7 - 4.2.1 (Indigo - Juno): http://download.eclipse.org/bpmn2-modeler/updates/juno <br>
    Eclipse 4.3 (Kepler): http://download.eclipse.org/bpmn2-modeler/updates/kepler <br>

三、 设置jbpm runtime路径 <br>
   window----preference---jbpm---installed jbpm Runtime,点击 add 添加jbpm-installer文件夹 -> lib中 -> jbpm-6.5.0.Final-bin解压后的路径 <br>
#### 流程图设计 
1. IDE-Plugin: JBPM PROCESS DIAGRAM（Eclipse）<br>
 ![Eclipse-BPMN插件](https://i.loli.net/2020/09/19/v7kgtIBTsbHcmfe.png)
2. WEB: JBPM在线流程设计器 <br>
 ![JBPM在线](https://i.loli.net/2020/09/19/IR64duGvnK9Q5rS.gif)
#### JBPM工作流项目Demo
一、 前提 <br>
1. 创建RuntimeManager对象 <br>


        RuntimeManager manager = createRuntimeManager("com/sample/sample.bpmn");

2. 创建RuntimeEngine对象 <br>


        RuntimeEngine engine = getRuntimeEngine(null);

3. 创建KieSession对象  <br>
     
     
        KieSession ksession = engine.getKieSession();

4. 创建TaskService对象 <br>


        TaskService taskService = engine.getTaskService();
        
二、启动流程 <br>

        ProcessInstance processInstance = ksession.startProcess("com.sample.bpmn.hello");
        assertProcessInstanceActive(processInstance.getId(), ksession);
		assertNodeTriggered(processInstance.getId(), "Task 1");
    
三、查询任务

        List<TaskSummary> list = taskService.getTasksAssignedAsPotentialOwner("john", "en-UK");
        TaskSummary task = list.get(0);
        System.out.println("John is executing task " + task.getName());

五、启动任务

		taskService.start(task.getId(), "john");
        
六、处理任务

		taskService.complete(task.getId(), "john", null);

       

